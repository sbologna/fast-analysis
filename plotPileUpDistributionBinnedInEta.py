''' Plots the average pile-up as a function of the genjeteta

  Requires a dataframe genJetEta as binning variable and a caloJetPileUp as sum2 and sumw2 weights

'''

from fast_plotter.utils import *
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl



plt.rc('text', usetex=True)
plt.rc('font', family='serif')
#plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))

datasets = {"CaloJet_PU200_Puppi_PFCandidates":"PFCandidates, Puppi", "CaloJet_PU200_Puppi_PFClusters":"PFClusters, Puppi"}

dataframe = read_binned_df(sys.argv[1])

#grouping by dataset and genjet eta
grouped_dataframe = dataframe.groupby(["dataset","absGenJetEta", "caloJetPileup"]).sum()
# replacing interval in genjet eta with central value
grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level="caloJetPileup", inplace=True)

#computing error for n
grouped_dataframe = grouped_dataframe.assign(n_err=(grouped_dataframe["n"]**(1./2.)).values)

# creating window and sub plot
#ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

figs = []
axs = []

for dataset, description in datasets.iteritems():
  fig = plt.figure()
  ax = fig.add_subplot(1, 1, 1)
  figs.append(fig)
  axs.append(ax)
  dataframe_plot = grouped_dataframe.loc[dataset].unstack("absGenJetEta")
  for col in dataframe_plot["n"].columns:
    col_total = dataframe_plot["n"][col].sum()
    # select every pileup range (:), select the column "n", and then subcolumn col
    dataframe_plot["n"][col] = dataframe_plot["n"][col]/col_total
    dataframe_plot["n_err"][col] = dataframe_plot["n_err"][col]/col_total

  dataframe_plot.plot(ax=ax, linestyle="None", marker=".", y="n", yerr="n_err")
  ax.annotate("CMS simulation", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(description, xycoords='axes fraction', xy=(0, 1.01))
  ax.set_xlabel("Pileup contribution (GeV)")
  ax.set_ylabel("a.u.")
  intervals = dataframe_plot["n"].columns
  ax.legend([
    (
      (str(x.left) if str(x.left) != "-inf" else "$-\\infty$")
      + " $ < \\eta < $ " + 
      (str(x.right) if str(x.right) != "inf" else "$\\infty$")
    ) for x in intervals
  ])
  fig.savefig("pileupDistributionBinnedInGenJetEta_" + dataset + ".png")
  fig.savefig("pileupDistributionBinnedInGenJetEta_" + dataset + ".svg")
  pkl.dump(fig,  open("pileupDistributionBinnedInGenJetEta_" + dataset + ".pickle",  'wb')  )

plt.show()