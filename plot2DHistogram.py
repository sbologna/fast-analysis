''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt andx_axis_varbinning variables and n

'''
from fast_plotter.utils import read_binned_df
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan
import seaborn as sns

def plot2DHistogram(*args, **kwargs):

  # plt.rc('text', usetex=True)
  plt.rc('font', family='serif', size=12)
  plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))
  plt.rcParams['font.family'] = 'sans-serif'
  plt.rcParams['font.sans-serif'] = 'Helvetica'
  datasetsAndTitle = {}
  for x in range(0, len(kwargs["datasets"])):
    datasetsAndTitle[kwargs["datasets"][x]] = kwargs["titles"][x]  

  # gen jet bin index to display
  saveFolder = kwargs["save_folder"]
  x_axis_var = kwargs["x_axis_var"]
  y_axis_var = kwargs["y_axis_var"]

  dataframe = read_binned_df(kwargs["dataframe_path"])
  apply_style = kwargs["style"]

  grouped_dataframe = dataframe.groupby(["dataset", x_axis_var, y_axis_var]).sum()
  # replacing interval with central value
  grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[1].mid, level=x_axis_var, inplace=True)
  grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level=y_axis_var, inplace=True)

  # creating window and sub plot
  #ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

  figs = []
  axs = []

  for dataset, description in datasetsAndTitle.items():
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    figs.append(fig)
    axs.append(ax)
    # preparing dataframe with columns to plot
    # selecting indexes
    legendList = []

    #trimming the dataframe
    dataframe_plot = grouped_dataframe.xs(dataset)
    #replacing NaNs (empty lines) with 0
    #removing under- and overflow, if existing, to have finite average
    if float("-inf") in dataframe_plot.index: dataframe_plot.loc[float("-inf")] = 0
    if float("+inf") in dataframe_plot.index: dataframe_plot.loc[float("+inf")] = 0
    dataframe_plot = dataframe_plot.unstack("puppiL1TMET")
    dataframe_plot = dataframe_plot.fillna(0)
    sns.heatmap(dataframe_plot, ax=ax)
  

    ax.annotate(description, xycoords='axes fraction', xy=(0.98, 0.60), horizontalalignment="right", verticalalignment="top")
    apply_style(ax)

    fig.savefig(saveFolder + "/plot2DHistogram_" + dataset + ".png")
    fig.savefig(saveFolder + "/plot2DHistogram_" + dataset + ".pdf")
    pkl.dump(fig, open(saveFolder + "/plot2DHistogram_" + dataset + ".pickle",  'wb')  )  

  if ("show" in kwargs) and (kwargs["show"]): plt.show()

if __name__ == "__main__":

  def format_plot_met(ax):
    ax.set_xlim([0, 1000])
    ax.set_ylim([0, 1000])
    ax.set_xticks([0], minor=True)
    ax.grid(True, which="minor", linestyle="dashed", linewidth=0.2, color="black", axis="x")
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"PUPPI $\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ L1T}}$", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"PF $\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ L1T}}$", horizontalalignment="right")
    return

  plot2DHistogram(
    datasets=[
      "MET_Puppi_vs_PF_PFCandidates_TTBar_PU200",
    ],
    titles=[
      "TTBar PU 200\n" + r"$|\eta| < 5$",
    ],
    save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
    dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.puppiL1TMET.pfL1TMET--PtDistributionBinner.csv",
    show=True,
    x_axis_var="puppiL1TMET",
    y_axis_var="pfL1TMET",
    style=format_plot_met
  )
