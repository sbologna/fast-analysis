import numpy as np; np.random.seed(3)
import pandas as pd
import seaborn.apionly as sns
import matplotlib.pyplot as plt

def get_data(n=266, s=[5,13]):
    val = np.c_[np.random.poisson(lam=s[0], size=n),
                np.random.poisson(lam=s[1], size=n)].T.flatten()
    comp = [s[0]]*n +  [s[1]]*n
    ov = np.random.choice(list("ABC"), size=2*n)
    return pd.DataFrame({"val":val, "overlap":ov, "comp":comp})

data1 = get_data(s=[9,11])
data2 = get_data(s=[7,19])
data3 = get_data(s=[1,27])

#option1 combine
for i, df in enumerate([data1,data2,data3]):
    df["data"] = ["data{}".format(i+1)] * len(df)

data = data1.append(data2)
data = data.append(data3)

import pdb; pdb.set_trace()

bw = 2
a = sns.FacetGrid(data, col="overlap",  hue="comp", row="data")
a = (a.map(sns.kdeplot, "val",bw=bw ))
plt.show()