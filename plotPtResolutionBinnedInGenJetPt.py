''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt and deltaPt binning variables and n

'''
from fast_plotter.utils import *
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan

def plotPtResolutionBinnedInGenJetPt(*args, **kwargs):

  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  #plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))
  datasetsAndTitle = {}
  for x in xrange(0, len(kwargs["datasets"])):
    datasetsAndTitle[kwargs["datasets"][x]] = kwargs["titles"][x]  

  # gen jet bin index to display
  binIdxs = kwargs["binIdxs"]
  saveFolder = kwargs["save_folder"]


  dataframe = read_binned_df(kwargs["dataframe_path"])

  #grouping by dataset and genjet eta
  grouped_dataframe = dataframe.groupby(["dataset","genJetPt", "deltaPt"]).sum()
  # replacing interval in genjet eta with central value
  grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level="deltaPt", inplace=True)

  #computing error for n
  grouped_dataframe = grouped_dataframe.assign(n_err=(grouped_dataframe["n"]**(1./2.)).values)

  # creating window and sub plot
  #ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

  figs = []
  axs = []

  for dataset, description in datasetsAndTitle.iteritems():
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    figs.append(fig)
    axs.append(ax)
    # preparing dataframe with columns to plot
    # selecting indexes
    idxs = [grouped_dataframe.index.levels[1][binIdx] for binIdx in binIdxs]
    #trimming the dataframe
    dataframe_plot = grouped_dataframe.loc[dataset].loc[idxs].unstack("genJetPt")
    intervals = dataframe_plot["n"].columns
    for col in intervals:
      col_total = dataframe_plot["n"][col].sum()
      dataframe_plot["n"][col] = dataframe_plot["n"][col]/col_total
      dataframe_plot["n_err"][col] = dataframe_plot["n_err"][col]/col_total

    dataframe_plot.plot(ax=ax, linestyle="None", marker=".", y="n", yerr="n_err", legend=False)
    ax.annotate("CMS simulation", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(description, xycoords='axes fraction', xy=(0, 1.01))
    ax.set_xlabel("$p_{t}^{L1T} - p_{t}^{gen}$ (GeV)")
    ax.set_ylabel("a.u.")
    
    legendList = []
    for col in intervals:
      dataframe_plot = dataframe_plot.fillna(0)
      #replacing NaNs (empty lines) with 0
      #removing under- and overflow, if existing, to have finite average
      if float("-inf") in dataframe_plot.index: dataframe_plot.loc[float("-inf")] = 0
      if float("+inf") in dataframe_plot.index: dataframe_plot.loc[float("+inf")] = 0
      col_total = grouped_dataframe.loc[dataset].loc[col]["n"].sum()
      delta_pt_avg = (dataframe_plot.index * dataframe_plot["n"][col]).sum()
      delta_pt_squared_avg = (dataframe_plot.index**2 * dataframe_plot["n"][col]).sum()
      #import pdb; pdb.set_trace()
      rms = round(sqrt(delta_pt_squared_avg - delta_pt_avg**2), 2)
      avg = round(delta_pt_avg, 2)
      relRMS = round(rms/col.mid, 2)*100
      legendEntry = (str(col.left) if str(col.left) != "-inf" else "$-\\infty$") + \
                    " $ < p_{t}^{gen} $ GeV $ < $ " + \
                    (str(col.right) if str(col.right) != "inf" else "$\\infty$") + \
                    ", n: " + str(col_total) + \
                    (", $\mu$: " + str(avg) if not isnan(avg) else "") + \
                    (", $\sigma$: " + str(rms) + " (" + str(relRMS) + "\%)" if not isnan(rms) else "")
      
      legendList.append(legendEntry)

    ax.legend(legendList)

    fig.savefig(saveFolder + "/ptResolutionBinnedInGenJetPt_" + dataset + ".png")
    fig.savefig(saveFolder + "/ptResolutionBinnedInGenJetPt_" + dataset + ".pdf")
    pkl.dump(fig, open(saveFolder + "/ptResolutionBinnedInGenJetPt_" + dataset + ".pickle",  'wb')  )  

  if ("show" in kwargs) and (kwargs["show"]): plt.show()

if __name__ == "__main__":
  # datasetsAndTitle = {
  #   "Calibrated_CaloJet_PU200_Puppi_PFClusters":"Phase-1, PFClusters, Puppi",
  #   "Calibrated_AK4Jet_PU200_Puppi_PFClusters":"AK4, PFClusters, Puppi",
  #   "Calibrated_CaloJet_PU200_Puppi_PFCandidates":"Phase-1, PFCandidates, Puppi",
  #   "Calibrated_AK4Jet_PU200_Puppi_PFCandidates":"AK4, PFCandidates, Puppi",
  #   }

  # # gen jet bin index to display
  # binIdxs = [3, 5, 10]

  # plotPtResolutionBinnedInGenJetPt(
  # datasets=["Calibrated_CaloJet_PU200_Puppi_PFCandidates", "Calibrated_AK4Jet_PU200_Puppi_PFCandidates"],
  # titles=["Histogrammed jet - $\eta < 1.4$", "AK4  - $\eta < 1.4$"],
  # binIdxs=[3,5,10],
  # save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_barrel/",
  # dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_barrel/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
  # show=True,
  # )

  # plotPtResolutionBinnedInGenJetPt(
  # datasets=["Calibrated_CaloJet_PU200_Puppi_PFCandidates", "Calibrated_AK4Jet_PU200_Puppi_PFCandidates"],
  # titles=["Histogrammed jet - $1.4 < \eta < 2.5$", "AK4 - $1.4 < \eta < 2.5$"],
  # binIdxs=[3,5,10],
  # save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_endcap/",
  # dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_endcap/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
  # show=True,
  # )

  # plotPtResolutionBinnedInGenJetPt(
  # datasets=["Calibrated_CaloJet_PU200_Puppi_PFCandidates", "Calibrated_AK4Jet_PU200_Puppi_PFCandidates"],
  # titles=["Histogrammed jet - $2.5 < \eta < 2.964$", "AK4 - $2.5 < \eta < 2.964$"],
  # binIdxs=[3,5,10],
  # save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_endcap_2/",
  # dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_endcap_2/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
  # show=True,
  # )

  # plotPtResolutionBinnedInGenJetPt(
  # datasets=["Uncalibrated_CaloJet_PU200_Puppi_PFCandidates", "Uncalibrated_AK4Jet_PU200_Puppi_PFCandidates"],
  # titles=["Histogrammed jet - $2.964 < \eta < 5$", "AK4 - $2.964 < \eta < 5$"],
  # binIdxs=[3,5,8],
  # save_folder="PU200_UncalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_forward/",
  # dataframe_path="PU200_UncalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_forward/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
  # show=True,
  # )

  plotPtResolutionBinnedInGenJetPt(
  datasets=["Calibrated_CaloJet_PU200_Puppi_PFCandidates", "Calibrated_AK4Jet_PU200_Puppi_PFCandidates"],
  titles=["Histogrammed jet - full detector", "AK4 - full detector"],
  binIdxs=[3,5,8],
  save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_full_detector/",
  dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_full_detector/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
  show=True,
  )

  pass