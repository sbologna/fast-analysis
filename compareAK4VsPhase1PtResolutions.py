#!/usr/bin/env python
''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt and deltaPt binning variables and n

'''

from fast_plotter.utils import *
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan

def compareAK4VsPhase1PtResolutions(*args, **kwargs):
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif', size=14)
  #plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))


  # gen jet bin index to display
  binIdx = kwargs["binIdx"]
  datasetsAndTitle = []
  for x in xrange(0, len(kwargs["dataset_pairs"])):
    datasetsAndTitle.append([kwargs["dataset_pairs"][x], kwargs["titles"][x]])
    
  legendLabels = kwargs["legend_labels"]
  saveFolder = kwargs["save_folder"]
  


  dataframe = read_binned_df(kwargs["dataframe_path"])

  #grouping by dataset and genjet eta
  grouped_dataframe = dataframe.groupby(["dataset","genJetPt", "response"]).sum()
  # replacing interval in genjet eta with central value
  grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level="response", inplace=True)

  #computing error for n
  grouped_dataframe = grouped_dataframe.assign(n_err=(grouped_dataframe["n"]**(1./2.)).values)

  ptInterval = grouped_dataframe.index.levels[1][binIdx]

  # creating window and sub plot
  #ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

  figs = []
  axs = []

  #selecting datasets of interest and pt interval of interest 

  for entry in datasetsAndTitle:
    datasets = entry[0]
    description = entry[1]
    dataframe_plot = grouped_dataframe.loc[(datasets)].xs(ptInterval, level=1)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    figs.append(fig)
    axs.append(ax)
    col_totals = []
    #normalising to area and computing errors 
    for dataset in datasets:
      col_total = dataframe_plot.loc[dataset]["n"].sum()
      col_totals.append(col_total)
      dataframe_plot.loc[dataset]["n"] = dataframe_plot.loc[dataset]["n"]/col_total
      dataframe_plot.loc[dataset]["n_err"] = dataframe_plot.loc[dataset]["n_err"]/col_total
    
    dataframe_plot.unstack("dataset").plot(ax=ax, linestyle="None", marker=".", y="n", yerr="n_err", legend=False)
    legendList = []
    
    description += "\n" + (str(ptInterval.left) if str(ptInterval.left) != "-inf" else "$-\\infty$") + \
    " $ < p_{T}^{gen} $ (GeV) $ < $ " + \
    (str(ptInterval.right) if str(ptInterval.right) != "inf" else "$\\infty$") 
    ax.annotate(description, xycoords='axes fraction', xy=(0.97, 0.72), horizontalalignment="right", verticalalignment="top")
    
    ax.annotate("CMS Preliminary Simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.annotate("QCD, $<$PU$>$=200", xycoords='axes fraction', xy=(0.99, 1.01), horizontalalignment="right")
    ax.set_xlabel("$p_{T}^{L1T}/p_{T}^{gen}$")
    ax.set_ylabel("a.u.")
    ax.set_ylim(-0.001, 0.023)
    RMScollection = []
    # building legend
    for datasetIdx in xrange(0, len(datasets)):
      dataset = datasets[datasetIdx]
      col_total = col_totals[datasetIdx]
      #replacing NaNs (empty lines) with 0
      df = dataframe_plot.loc[dataset].fillna(0)
      #removing under- and overflow, if existing, to have finite average
      if float("-inf") in df.index: df.loc[float("-inf")] = 0
      if float("+inf") in df.index: df.loc[float("+inf")] = 0
      # building a legend entry with mean, rms and number of entries
      delta_pt_avg = (df.index * df["n"]).sum()
      delta_pt_squared_avg = (df.index**2 * df["n"]).sum()
      rms = round(sqrt(delta_pt_squared_avg - delta_pt_avg**2), 2)
      avg = round(delta_pt_avg, 2)
      relRMS = rms/ptInterval.mid*100
      RMScollection.append(relRMS)
      rms = round(rms, 2)
      relRMS = round(relRMS, 2)
      if dataset == datasets[0]:
        legendEntry = legendLabels[0]
      else:
        legendEntry = legendLabels[1] 
      
      # legendEntry += ", n: " + str(col_total) + \
      #               (", $\mu$: " + str(avg) if not isnan(avg) else "") + \
      #               (", $\sigma$: " + str(rms) + " (" + str(relRMS) + "\%)" if not isnan(rms) else "")
        
      legendList.append(legendEntry)

    rmsDiff = round((RMScollection[1] - RMScollection[0])/RMScollection[0] * 100, 2)
    ax.annotate("$\Delta$RMS\%=" + str(rmsDiff) + "\%", xycoords='axes fraction', xy=(0.97, 0.79), horizontalalignment="right", verticalalignment="top")

    ax.legend(legendList)

    # Removes xaxis ticks and scale
    ax.set_xticklabels([])
    ax.set_xticks([])

    fig.savefig(saveFolder + "/ptResolution_AK4VsPhase1_" + datasets[0] + "_" + datasets[1] + ".png")
    fig.savefig(saveFolder + "/ptResolution_AK4VsPhase1_" + datasets[0] + "_" + datasets[1] + ".eps")
    pkl.dump(fig, open(saveFolder + "/ptResolution_AK4VsPhase1_" + datasets[0] + "_" + datasets[1] + ".pickle",  'wb')  )  

  if ("show" in kwargs) and (kwargs["show"]): plt.show()


if __name__ == "__main__":
  compareAK4VsPhase1PtResolutions(
    binIdx = 8,
    dataset_pairs = [
      ["Calibrated_AK4Jet_PU200_Puppi_PFClusters", "Calibrated_CaloJet_PU200_Puppi_PFClusters"],
      ["Calibrated_AK4Jet_PU200_Puppi_PFCandidates", "Calibrated_CaloJet_PU200_Puppi_PFCandidates"],
    ],
    titles = [
      "PFClusters \n$1.4 < |\eta| < 2.5$",
      "PFCandidates, Puppi \n$1.4 < |\eta| < 2.5$",
    ],
    legend_labels = [
      "Anti-$k_t$ R=0.4",
      "Phase-1 5x5"
    ],
    save_folder = "poster_pic/endcap",
    dataframe_path = "PU200_new_calibration_fine_granularity_0.4Squares_endcap/tbl_dataset.genJetPt.absGenJetEta.response.csv",
  )

  compareAK4VsPhase1PtResolutions(
    binIdx = 8,
    dataset_pairs = [
      ["Calibrated_AK4Jet_PU200_Puppi_PFClusters", "Calibrated_CaloJet_PU200_Puppi_PFClusters"],
      ["Calibrated_AK4Jet_PU200_Puppi_PFCandidates", "Calibrated_CaloJet_PU200_Puppi_PFCandidates"],
    ],
    titles = [
      "PFClusters \n$|\eta| < 1.4$",
      "PFCandidates, Puppi \n$|\eta| < 1.4$",
    ],
    legend_labels = [
      "Anti-$k_t$ R=0.4",
      "Phase-1 5x5"
    ],
    save_folder = "poster_pic/barrel",
    dataframe_path = "PU200_new_calibration_fine_granularity_0.4Squares_barrel/tbl_dataset.genJetPt.absGenJetEta.response.csv",
    show = True
  )
  # binIdx = kwargs["binIdx"]
  # legendLabels = [
  #   "AK4",
  #   "Phase-1"
  # ]
  # datasetsAndTitle = [
  #   [["Calibrated_AK4Jet_PU200_Puppi_PFClusters", "Calibrated_CaloJet_PU200_Puppi_PFClusters"], "PFClusters"],
  #   [["Calibrated_AK4Jet_PU200_Puppi_PFCandidates", "Calibrated_CaloJet_PU200_Puppi_PFCandidates"], "PFCandidates, Puppi"],
  # ]