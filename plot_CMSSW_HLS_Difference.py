''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt andx_axis_varbinning variables and n

'''
from fast_plotter.utils import read_binned_df
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan
from plotVariableBinnedInAnotherVariable import plotVariableBinnedInAnotherVariable

########################
#         MET          #
########################

def format_plot_met_comparison(ax):
  ax.set_xlim([-1, 10])
  ax.set_ylim([0.1, 30000])
  ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
  ax.annotate(r"14 TeV, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.075)
  ax.set_xlabel(r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ CMSSW}} - \mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ HLS}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.11, 0.95)
  ax.set_ylabel("# events")
  ax.set_yscale("log")
  return

plotVariableBinnedInAnotherVariable(
  # datasets you want to plot from
  datasets=[
    "HLS_CMSSW_Sum_Comparison",
  ],
  # plot title and info
  titles=[
    "TTBar\n50000 events\n"+r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ CMSSW}} > 0$",
  ],
  # save folder for plots
  save_folder="CMSSW_HLS_Comparison/METComparison_noZeroMET",
  # path of dataframe containing the stuff to plot
  dataframe_path="CMSSW_HLS_Comparison/METComparison_noZeroMET/tbl_dataset.metDifference_HLS_CMSSW--METDiffDistributionBinner.csv",
  # x axis and primary binning variable
  x_axis_var="metDifference_HLS_CMSSW",
  # additional cosmetics to plot
  style=format_plot_met_comparison
)

########################
#         HT           #
########################

# def format_plot_ht_comparison(ax):
#   ax.set_xlim([-1, 1])
#   ax.set_ylim([0, 60000])
#   ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
#   ax.annotate(r"14 TeV, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
#   ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
#   ax.xaxis.set_label_coords(0.98, -0.075)
#   ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}^{\mathrm{CMSSW}} - \mathrm{H}_{\mathrm{T}}^{\mathrm{HLS}}$ (GeV)", horizontalalignment="right")
#   ax.yaxis.set_label_coords(-0.1, 0.95)
#   ax.set_ylabel("a.u.")
#   return

# plotVariableBinnedInAnotherVariable(
#   # datasets you want to plot from
#   datasets=[
#     "HLS_CMSSW_Sum_Comparison",
#   ],
#   # plot title and info
#   titles=[
#     "TTBar\n50000 events",
#   ],
#   # save folder for plots
#   save_folder="CMSSW_HLS_Comparison/HTComparison",
#   # path of dataframe containing the stuff to plot
#   dataframe_path="CMSSW_HLS_Comparison/tbl_dataset.htDifference_HLS_CMSSW--HTDiffDistributionBinner.csv",
#   # x axis and primary binning variable
#   x_axis_var="htDifference_HLS_CMSSW",
#   # additional cosmetics to plot
#   style=format_plot_ht_comparison
# )

########################
#         MHT          #
########################

# def format_plot_mht_comparison(ax):
#   ax.set_xlim([-1, 1])
#   ax.set_ylim([0, 60000])
#   ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
#   ax.annotate(r"14 TeV, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
#   ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
#   ax.xaxis.set_label_coords(0.98, -0.075)
#   ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}^{\mathrm{miss CMSSW}} - \mathrm{H}_{\mathrm{T}}^{\mathrm{miss HLS}}$ (GeV)", horizontalalignment="right")
#   ax.yaxis.set_label_coords(-0.1, 0.95)
#   ax.set_ylabel("a.u.")
#   return

# plotVariableBinnedInAnotherVariable(
#   # datasets you want to plot from
#   datasets=[
#     "HLS_CMSSW_Sum_Comparison",
#   ],
#   # plot title and info
#   titles=[
#     "TTBar\n50000 events",
#   ],
#   # save folder for plots
#   save_folder="MHT_analysis/",
#   # path of dataframe containing the stuff to plot
#   dataframe_path="MHT_analysis/tbl_dataset.mhtDifference_HLS_CMSSW--MHTDiffDistributionBinner.csv",
#   # x axis and primary binning variable
#   x_axis_var="mhtDifference_HLS_CMSSW",
#   # additional cosmetics to plot
#   style=format_plot_mht_comparison
# )
