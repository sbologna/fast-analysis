#!/usr/bin/env python

''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt and deltaPt binning variables and n

'''

from fast_plotter.utils import *
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan

def compareResolutionWithAllSettings(*args, **kwargs):
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  # plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))


  # gen jet bin index to display
  binIdx = kwargs["binIdx"]
  datasets = kwargs["datasets"]
  legendLabels = kwargs["legend_labels"]
  saveFolder = kwargs["save_folder"]
  plotName = kwargs["plotName"]
  
  dataframes = [read_binned_df(df_path) for df_path in kwargs["dataframe_paths"]]

  # grouping by dataset and genjet eta
  grouped_dataframes = [dataframe.groupby(["dataset","genJetPt", "deltaPt"]).sum() for dataframe in dataframes]

  # building a unique dataframe with all the data to plot
  for index in xrange(0, len(datasets)):
    grouped_dataframes[index] = grouped_dataframes[index].loc[[datasets[index]]]
    grouped_dataframes[index].rename(index={datasets[index]: datasets[index] + "_" + str(index)}, inplace=True)
    if index == 0:
      grouped_dataframe = grouped_dataframes[0]
    else:
      grouped_dataframe = grouped_dataframe.append(grouped_dataframes[index])

  # replacing interval in genjet eta with central value

  grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level="deltaPt", inplace=True)

  #computing error for n
  grouped_dataframe = grouped_dataframe.assign(n_err=(grouped_dataframe["n"]**(1./2.)).values)

  ptInterval = grouped_dataframe.index.levels[1][binIdx]

  # creating window and sub plot
  # ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

  figs = []
  axs = []

  # selecting pt interval of interest 
  dataframe_plot = grouped_dataframe.xs(ptInterval, level=1)

  fig = plt.figure()
  ax = fig.add_subplot(1, 1, 1)
  figs.append(fig)
  axs.append(ax)
  col_totals = []
  # normalising to area and computing errors 
  for dataset in dataframe_plot.index.levels[0]:
    col_total = dataframe_plot.loc[dataset]["n"].sum()
    col_totals.append(col_total)
    dataframe_plot.loc[dataset]["n"] = dataframe_plot.loc[dataset]["n"]/col_total
    dataframe_plot.loc[dataset]["n_err"] = dataframe_plot.loc[dataset]["n_err"]/col_total
  
  dataframe_plot.unstack("dataset").plot(ax=ax, linestyle="None", marker=".", y="n", yerr="n_err", legend=False)
  legendList = []
  ax.annotate("CMS simulation", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  description = kwargs["title"] + " - " + str(ptInterval.left) + " $< p_t$ (GeV) $<$ " + str(ptInterval.right)
  (str(ptInterval.right) if str(ptInterval.right) != "inf" else "$\\infty$") 
  ax.annotate(description, xycoords='axes fraction', xy=(0, 1.01))
  ax.set_xlabel("$p_{t}^{L1T} - p_{t}^{gen}$ (GeV)")
  ax.set_ylabel("a.u.")
  # building legend
  for datasetIdx in xrange(0, len(datasets)):
    dataset = dataframe_plot.index.levels[0][datasetIdx]
    col_total = col_totals[datasetIdx]
    # replacing NaNs (empty lines) with 0
    df = dataframe_plot.loc[dataset].fillna(0)
    # removing under- and overflow, if existing, to have finite average
    if float("-inf") in df.index: df.loc[float("-inf")] = 0
    if float("+inf") in df.index: df.loc[float("+inf")] = 0
    # building a legend entry with mean, rms and number of entries
    delta_pt_avg = (df.index * df["n"]).sum()
    delta_pt_squared_avg = (df.index**2 * df["n"]).sum()
    rms = round(sqrt(delta_pt_squared_avg - delta_pt_avg**2), 2)
    avg = round(delta_pt_avg, 2)
    relRMS = round(rms/ptInterval.mid, 2)*100
    
    legendEntry = legendLabels[datasetIdx] + ", n: " + str(col_total) + \
                  (", $\mu$: " + str(avg) if not isnan(avg) else "") + \
                  (", $\sigma$: " + str(rms) + " (" + str(relRMS) + "\%)" if not isnan(rms) else "")
      
    legendList.append(legendEntry)

  ax.legend(legendList)
  
  fig.savefig(saveFolder + "/" + plotName + ".png")
  fig.savefig(saveFolder + "/" + plotName + ".pdf")
  pkl.dump(fig, open(saveFolder + "/" + plotName + ".pickle",  'wb')  )  

  if ("show" in kwargs) and (kwargs["show"]): plt.show()


if __name__ == "__main__":
  compareResolutionWithAllSettings(
    binIdx = 3,
    datasets = [
      "Calibrated_AK4Jet_PU200_Puppi_PFClusters",
      "Calibrated_CaloJet_PU200_Puppi_PFClusters",
      "Calibrated_CaloJet_PU200_Puppi_PFClusters",
      "Calibrated_CaloJet_PU200_Puppi_PFClusters_PileUpSubtraction",
    ],
    title = "PFClusters, $2.5 < |\eta| < 3$",
    legend_labels = [
      "AK4",
      "Phase-1",
      "0.4 square, 5x5",
      "Phase-1 w/ PU subtraction"
    ],
    save_folder = "comparison_plots",
    dataframe_paths = [
      "PU200_new_calibration_endcap_2/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
      "PU200_new_calibration_endcap_2/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
      "PU200_new_calibration_fine_granularity_0.4Squares_endcap_2/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
      "PU200_new_calibration_pileupsubtraction_endcap_2/tbl_dataset.genJetPt.absGenJetEta.deltaPt.caloJetPileup.csv",
    ],
    show=True,
    plotName = "ptResolution_Endcap2_Comparison"
  )