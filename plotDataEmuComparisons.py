#!/usr/bin/env python
''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt and deltaPtOverPt binning variables and n

'''

from fast_plotter.utils import read_binned_df
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan
from compareDatasets import compareDatasets

def format_plot_ht_comparison(ax):
  ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.055)
  ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}$ (GeV) ", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel(r"a.u.")
  # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  ax.set_xlim([25, 250])
  ax.set_ylim([1, 1000])
  ax.set_yscale("log")

compareDatasets(
  x_axis_var = "ht",
  dataset_list = [
    "Sums",
    "Sums",
  ],
  title= "",
  # title = "TTbar\n50000 events",
  legend_labels = [
    r"Emu",
    r"HW",
  ],
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.ht--emuHTDistributionBinner.csv",
    "HW_EMU_Comp_0.15/tbl_dataset.ht--hwHTDistributionBinner.csv",
  ],
  style=format_plot_ht_comparison,
  plotID = "HT_Distribution_Comparison",
  include_ratio = True
)

def format_plot_met_comparison(ax):
  ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.055)
  ax.set_xlabel(r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel(r"a.u.")
  # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  ax.set_xlim([4, 150])
  ax.set_ylim([10, 10000])
  ax.set_yscale("log")

compareDatasets(
  x_axis_var = "met",
  dataset_list = [
    "Sums",
    "Sums",
  ],
  title= "",
  # title = "TTBar\n50000 events",
  legend_labels = [
    r"Emu",
    r"HW",
  ],
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.met--emuMETDistributionBinner.csv",
    "HW_EMU_Comp_0.15/tbl_dataset.met--hwMETDistributionBinner.csv",
  ],
  style=format_plot_met_comparison,
  plotID = "MET_Distribution_Comparison",
  include_ratio = True
)

def format_plot_mht_comparison(ax):
  ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.055)
  ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}^{\mathrm{miss}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel(r"a.u.")
  # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  ax.set_xlim([4, 150])
  ax.set_ylim([1, 3500])
  ax.set_yscale("log")

compareDatasets(
  x_axis_var = "mht",
  dataset_list = [
    "Sums",
    "Sums",
  ],
  title= "",
  # title = "TTBar\n50000 events",
  legend_labels = [
    r"Emu",
    r"HW",
  ],
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.mht--emuMHTDistributionBinner.csv",
    "HW_EMU_Comp_0.15/tbl_dataset.mht--hwMHTDistributionBinner.csv",
  ],
  style=format_plot_mht_comparison,
  plotID = "MHT_Distribution_Comparison",
  include_ratio = True
)

def format_plot_ht_difference(ax):
  ax.set_xlim([-10, 10])
  ax.set_ylim([0.5, 60000])
  ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
  ax.annotate(r"14 TeV, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.075)
  ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}^{\mathrm{emu}} - \mathrm{H}_{\mathrm{T}}^{\mathrm{hw}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel("a.u.")
  # ax.set_yscale("log")
  return

compareDatasets(
  x_axis_var = "HTdiff",
  binning_var = "emuHT",
  binning_var_label=r"$\mathrm{H}_{\mathrm{T}} (GeV)$",
  binIdx = 1,
  dataset_list = [
    "Sums",
  ],
  title= "",
  # title = "TTbar\n50000 events",
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.emuHT.HTdiff--HTDiffDistributionBinner.csv",
  ],
  style=format_plot_ht_difference,
  plotID = "HT_Difference_Distribution",
)

def format_plot_met_difference(ax):
  ax.set_xlim([-10, 10])
  ax.set_ylim([0.1, 30000])
  ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
  ax.annotate(r"14 TeV, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.075)
  ax.set_xlabel(r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ emu}} - \mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ hw}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.11, 0.95)
  ax.set_ylabel("# events")
  # ax.set_yscale("log")
  return

compareDatasets(
  x_axis_var = "METdiff",
  binning_var = "emuMET",
  binning_var_label=r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss}} (GeV)$",
  binIdx = 1,
  dataset_list = [
    "Sums",
  ],
  title= "",
  # title = "TTBar\n50000 events",
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.emuMET.METdiff--METDiffDistributionBinner.csv",
  ],
  style=format_plot_met_difference,
  plotID = "MET_Difference_Distribution"
)

def format_plot_mht_difference(ax):
  ax.set_xlim([-1, 10])
  ax.set_ylim([0.5, 30000])
  ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
  ax.annotate(r"14 TeV, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.075)
  ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}^{\mathrm{miss emu}} - \mathrm{H}_{\mathrm{T}}^{\mathrm{miss hw}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel("a.u.")
  return

compareDatasets(
  x_axis_var = "MHTdiff",
  dataset_list = [
    "Sums",
  ],
  title= "",
  # title = "TTBar\n50000 events",
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.emuMHT.MHTdiff--MHTDiffDistributionBinner.csv",
  ],
  style=format_plot_mht_difference,
  plotID = "MHT_Difference_Distribution",
)

def format_plot_pt_comparison(ax):
  ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.055)
  ax.set_xlabel(r"$p_{\mathrm{T}}$ (GeV)", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel(r"a.u.")
  # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  ax.set_xlim([0, 299])
  ax.set_ylim([1, 3500])
  ax.set_yscale("log")

compareDatasets(
  x_axis_var = "pt",
  dataset_list = [
    "EmulatorJets",
    "HardwareJets",
  ],
  title = "",
  # title = "TTBar\n50000 events",
  legend_labels = [
    r"Emu",
    r"HW",
  ],
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.pt--ptDistributionBinner.csv",
    "HW_EMU_Comp_0.15/tbl_dataset.pt--ptDistributionBinner.csv",
  ],
  style=format_plot_pt_comparison,
  plotID = "pt_Distribution_Comparison",
  include_ratio = True
)

def format_plot_phi_comparison(ax):
  ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.055)
  ax.set_xlabel(r"$\phi$", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel(r"a.u.")
  # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  ax.set_xlim([0, 0.69])
  ax.set_ylim([1, 3500])
  # ax.set_yscale("log")

compareDatasets(
  x_axis_var = "phi",
  dataset_list = [
    "EmulatorJets",
    "HardwareJets",
  ],
  title = "",
  # title = "TTBar\n50000 events",
  legend_labels = [
    r"Emu",
    r"HW",
  ],
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.phi--phiDistributionBinner.csv",
    "HW_EMU_Comp_0.15/tbl_dataset.phi--phiDistributionBinner.csv",
  ],
  style=format_plot_phi_comparison,
  plotID = "phi_Distribution_Comparison",
  include_ratio = True
)

def format_plot_eta_comparison(ax):
  ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  ax.xaxis.set_label_coords(0.98, -0.055)
  ax.set_xlabel(r"$\eta$", horizontalalignment="right")
  ax.yaxis.set_label_coords(-0.1, 0.95)
  ax.set_ylabel(r"a.u.")
  # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  ax.set_xlim([0, 1.49])
  ax.set_ylim([1, 1500])
  # ax.set_yscale("log")

compareDatasets(
  x_axis_var = "eta",
  dataset_list = [
    "EmulatorJets",
    "HardwareJets",
  ],
  title = "",
  # title = "TTBar\n50000 events",
  legend_labels = [
    r"Emu",
    r"HW",
  ],
  save_folder = "HW_EMU_Comp_0.15",
  dataframe_paths = [
    "HW_EMU_Comp_0.15/tbl_dataset.eta--etaDistributionBinner.csv",
    "HW_EMU_Comp_0.15/tbl_dataset.eta--etaDistributionBinner.csv",
  ],
  style=format_plot_eta_comparison,
  plotID = "eta_Distribution_Comparison",
  include_ratio = True,
  show = True
)