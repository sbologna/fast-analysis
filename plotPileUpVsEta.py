''' Plots the average pile-up as a function of the genjeteta

  Requires a dataframe genJetEta as binning variable and a caloJetPileUp as sum2 and sumw2 weights

'''

from fast_plotter.utils import *
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl


#plt.rc('text', usetex=True)
plt.rc('font', family='serif')
#plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))

datasets = {"CaloJet_PU200_Puppi_PFCandidates":"r", "CaloJet_PU200_Puppi_PFClusters":"b"}

dataframe = read_binned_df(sys.argv[1])

#grouping by dataset and genjet eta
grouped_dataframe = dataframe.groupby(["dataset","genJetEta"]).sum()
# replacing interval in genjet eta with central value
grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[1].mid, level=1, inplace=True)

# computing average pileup and adding it as a column
grouped_dataframe = grouped_dataframe.assign(averagePileup=(grouped_dataframe["caloJetPileup:sumw"]/grouped_dataframe["n"]).values)
# computing error on average pileup and adding it as a column
grouped_dataframe = grouped_dataframe.assign(averagePileupError=(((grouped_dataframe["caloJetPileup:sumw2"]/grouped_dataframe["n"]) - grouped_dataframe["averagePileup"]**2).values)**(1./2.))
grouped_dataframe = grouped_dataframe.reset_index(level="genJetEta")

# creating window and sub plot
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))


for dataset, color in datasets.iteritems():
  grouped_dataframe.loc[dataset].plot.scatter(x="genJetEta", y="averagePileup", yerr="averagePileupError", ax=ax, color=color)

#df1 = dataframe.groupby(["genJetPt", "deltaPt"]).sum().loc[0]
#df2 = dataframe.groupby(["genJetPt", "deltaPt"]).sum().loc[40]

ax.legend(datasets.keys())
fig.savefig("pileupVsEtaDistribution_" + dataset + ".png")
fig.savefig("pileupVsEtaDistribution_" + dataset + ".svg")
pkl.dump(fig,  open("pileupVsEtaDistribution_" + dataset + ".pickle",  'wb')  )

ax.annotate("CMS simulation", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")

fig.show()