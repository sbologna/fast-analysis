''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt andx_axis_varbinning variables and n

'''
from fast_plotter.utils import read_binned_df
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan

#function that plots a variable distribution binned in another variables as a set of 1d plots

def plotVariableBinnedInAnotherVariable(*args, **kwargs):

  #setting up font 
  plt.rc('font', family='serif', size=12)
  plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))
  plt.rcParams['font.family'] = 'sans-serif'
  plt.rcParams['font.sans-serif'] = 'Helvetica'
  
  #retrieving dataset and plot title
  datasetsAndTitle = {}
  for x in range(0, len(kwargs["datasets"])):
    datasetsAndTitle[kwargs["datasets"][x]] = kwargs["titles"][x]  

  # gen jet bin index to display
  binIdxs = kwargs["binIdxs"] if "binIdxs" in kwargs else None
  saveFolder = kwargs["save_folder"]
  x_axis_var = kwargs["x_axis_var"]

  dataframe = read_binned_df(kwargs["dataframe_path"])
  apply_style = kwargs["style"]

  #select eta range, if any, otherwise go full detector
  if "region_index" in kwargs:
    etaInterval = dataframe.index.levels[2][kwargs["region_index"]]
    dataframe = dataframe.xs(etaInterval, level=2)

  #grouping by dataset and genjet eta
  if binIdxs is not None:
    grouped_dataframe = dataframe.groupby(["dataset", kwargs["binning_var"], x_axis_var]).sum()
    grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level=x_axis_var, inplace=True)
  else:
    grouped_dataframe = dataframe.groupby(["dataset", x_axis_var]).sum()
    grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[1].mid, level=x_axis_var, inplace=True)
  # replacing interval in genjet eta with central value

  # creating window and sub plot
  #ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

  figs = []
  axs = []

  # creating a plot for each dataset
  for dataset, description in datasetsAndTitle.items():
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    figs.append(fig)
    axs.append(ax)
    # preparing dataframe with columns to plot
    # selecting bins to plot, if any otherwise plot without binning in the secondary variable
    if binIdxs is not None:
      idxs = [grouped_dataframe.index.levels[1][binIdx] for binIdx in binIdxs]
    else:
      idxs = [None]
    
    legendList = []
    #plotting all the required bins for a specific plot
    for intervals in idxs:
      #trimming the dataframe
      #choosing the dataset
      dataframe_plot = grouped_dataframe.xs(dataset)
      # selecting the first bin
      if intervals is not None:
        dataframe_plot = dataframe_plot.xs(intervals)
      # normalising to the area
      if ("normalise" in kwargs) and (kwargs["normalise"]):
        col_total = dataframe_plot["n"].sum()
        dataframe_plot["n"] = dataframe_plot["n"]/col_total
      dataframe_plot = dataframe_plot.fillna(0)
      # dataframe_plot.plot(ax=ax, linestyle="None", marker=".", y="n", yerr="n_err", legend=False)
      #plotting as a step plot
      ax.step(dataframe_plot.index.values, dataframe_plot["n"], where="pre") 
    
      # computing average and rms
      #replacing NaNs (empty lines) with 0
      #removing under- and overflow, if existing, to have finite average
      # import pdb ; pdb.set_trace()
      if float("-inf") in dataframe_plot.index: dataframe_plot.loc[float("-inf")] = 0
      if float("+inf") in dataframe_plot.index: dataframe_plot.loc[float("+inf")] = 0
      # delta_pt_avg = (dataframe_plot.index * dataframe_plot["n"]).sum()
      # delta_pt_squared_avg = (dataframe_plot.index**2 * dataframe_plot["n"]).sum()
      # import pdb; pdb.set_trace()
      # rms = round(sqrt(delta_pt_squared_avg - delta_pt_avg**2), 2)
      # avg = round(delta_pt_avg, 2)
      # building legend
      if intervals is not None:
        legendEntry = (str(intervals.left) if str(intervals.left) != "-inf" else "$-\\infty$") + \
                      " $ < $" + kwargs["binning_var_label"] + " $ < $ " + \
                      (str(intervals.right) if str(intervals.right) != "inf" else "$\\infty$")
                      # ", n: " + str(col_total) + \
                      # (", $\mu$: " + str(avg) if not isnan(avg) else "") + \
                      # (", $\sigma$: " + str(rms))
        legendList.append(legendEntry)
    
    # adding title to plot
    ax.annotate(description, xycoords='axes fraction', xy=(0.98, 0.60), horizontalalignment="right", verticalalignment="top")
    if len(legendList) > 0: ax.legend(legendList, labelspacing=0.3, framealpha=1)
    #applying addtional style rules passed via apply style
    apply_style(ax)

    fig.savefig(saveFolder + "/plot_" + dataset + ".png")
    fig.savefig(saveFolder + "/plot_" + dataset + ".pdf")
    pkl.dump(fig, open(saveFolder + "/plot_" + dataset + ".pickle",  'wb')  )  

  if ("show" in kwargs) and (kwargs["show"]): plt.show()

if __name__ == "__main__":

  def format_plot_met(ax):
    ax.set_xlim([-1, 2])
    ax.set_ylim([0, 0.2])
    ax.axvline(x=0, linestyle="dashed", linewidth=0.2, color="black")
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"($\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ L1T}}$ $-$ $\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$)/$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$ ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel("a.u.")
    return

  plotVariableBinnedInAnotherVariable(
    # datasets you want to plot from
    datasets=[
      "MET_Puppi_PFCandidates_7x7_TTBar_PU200",
    ],
    # plot title and info
    titles=[
      "TTBar, 200 PU\n" + r"PUPPI, $|\eta| < 5$" + "\nBinned sin-cos",
    ],
    # secondary binning indices, plots for all the binning indices if unspecified
    binIdxs=[1, 2, 3],
    # save folder for plots
    save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
    # path of dataframe containing the stuff to plot
    dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genMET.deltaPtOverPt--DeltaPtOverPtBinner.csv",
    # controls if the plot will immediately displayed
    show=True,
    # secondary binning variable
    binning_var="genMET",
    # name of secondary binning variable for legend
    binning_var_label=r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$ (GeV)",
    # x axis and primary binning variable
    x_axis_var="deltaPtOverPt",
    # additional cosmetics to plot
    style=format_plot_met
  )
