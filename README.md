# FAST

A bunch of scripts that I use to bin and plot stuff with FAST, matplotlib, and PANDAS.

## Data-emu comparison plots

On sc01:

```bash
source /software/fast-hep/bin/activate /software/fast-hep/envs/fast
```

Make sure you setup ```Dataset_HW_EMU_Sums_Comparison.yaml``` and ```Dataset_HW_EMU_Jet_Comparison.yaml ``` to point to the correct file and run:
```bash
fast_carpenter --outdir <OUTPUT_DIRECTORY> Dataset_HW_EMU_Sums_Comparison.yaml sequence_build_hw_emu_sums_cfg.yaml
fast_carpenter --outdir <OUTPUT_DIRECTORY> Dataset_HW_EMU_Jet_Comparison.yaml sequence_build_hw_emu_jets_cfg.yaml
```

Plot with:
```bash
python plotDataEmuComparisons.py
```