#!/usr/bin/env python
''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt and deltaPtOverPt binning variables and n

'''

from fast_plotter.utils import read_binned_df
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan

def compareDatasets(*args, **kwargs):
  # plt.rc('text', usetex=True)
  plt.rc('font', size=12)
  plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y', 'black'])))
  plt.rcParams['font.family'] = 'sans-serif'
  plt.rcParams['font.sans-serif'] = 'Helvetica'
  # gen jet bin index to display
  binIdx = kwargs["binIdx"] if "binIdx" in kwargs else None
  saveFolder = kwargs["save_folder"]
  x_axis_var = kwargs["x_axis_var"]

  apply_style = kwargs["style"]
  
  include_ratio = kwargs["include_ratio"] if "include_ratio" in kwargs else False

  if include_ratio:
    fig = plt.figure(figsize=[6.4, 6.4])
    axs = fig.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]})
    ax=axs[0]
    ax_ratio=axs[1]
    dataframe_to_compare = []
  else:
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

  legendList = []

  dataframes = []

  title = kwargs["title"]

  for dataframe_path in kwargs["dataframe_paths"]:
    dataframes.append(read_binned_df(dataframe_path))

  for x, dataframe in enumerate(dataframes):

    if binIdx is not None:
      #grouping by dataset and genjet eta
      grouped_dataframe = dataframe.groupby(["dataset", kwargs["binning_var"], x_axis_var]).sum()
      # second series should show dots in the middle
      if x == 1 and include_ratio:
        grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level=x_axis_var, inplace=True)
      else:
        grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].left, level=x_axis_var, inplace=True)
    else: 
      grouped_dataframe = dataframe.groupby(["dataset", x_axis_var]).sum()
      # second series should show dots in the middle
      if x == 1 and include_ratio:
        grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[1].mid, level=x_axis_var, inplace=True)
      else:
        grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[1].right, level=x_axis_var, inplace=True)
    # replacing interval in genjet eta with central value

    #computing error for n
    grouped_dataframe = grouped_dataframe.assign(n_err=(grouped_dataframe["n"]**(1./2.)).values)

    # creating window and sub plot
    #ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))


    # preparing dataframe with columns to plot
    # selecting indexes
    col = grouped_dataframe.index.levels[1][binIdx] if binIdx is not None else None
    #trimming the dataframe
    dataframe_plot = grouped_dataframe.loc[kwargs["dataset_list"][x]]
    if col is not None: dataframe_plot = dataframe_plot.xs(col)
    #replacing NaNs (empty lines) with 0
    if ("normalise" in kwargs) and (kwargs["normalise"]):
      col_total = dataframe_plot["n"].sum()
      dataframe_plot["n"] = dataframe_plot["n"]/col_total
    dataframe_plot = dataframe_plot.fillna(0)
    if include_ratio:
      # plot first plot as step
      if x == 0:
        ax.step(dataframe_plot.index.values, dataframe_plot["n"], where="pre")
      #plot second as scatter
      else:
        ax.scatter(dataframe_plot.index.values, dataframe_plot["n"], color="black", s=10, marker="o")
      dataframe_to_compare.append(dataframe_plot)
    else:
      ax.step(dataframe_plot.index.values, dataframe_plot["n"], where="pre")

    #do only once
    if x == 0:
      if col is not None:
        ptBinAnnotation = (str(col.left) if str(col.left) != "-inf" else "$-\\infty$") + \
                        r" $<$ " + kwargs["binning_var_label"] + " $<$ " + \
                        (str(col.right) if str(col.right) != "inf" else "$\\infty$")
        ax.annotate(title + "\n" + ptBinAnnotation, xycoords='axes fraction', xy=(0.99, 0.55), horizontalalignment="right")
      else: 
        ax.annotate(title, xycoords='axes fraction', xy=(0.99, 0.55), horizontalalignment="right")

    if "legend_labels" in kwargs:
      legendList.append(kwargs["legend_labels"][x])

  apply_style(ax)

  if len(legendList): ax.legend(legendList, labelspacing=0.3, framealpha=1)

  if include_ratio:
    ax_ratio.scatter(dataframe_to_compare[1].index.values,  dataframe_to_compare[0]["n"].values/dataframe_to_compare[1]["n"].values  ,  c='#000000', linewidths=0.5, s=10, zorder=1)
    ax_ratio.axhline(y=1, linewidth=1, linestyle="--", c="#929591")
    ax_ratio.set_ylim(0.5,1.5)
    ax_ratio.set_xlim(ax.get_xlim())
    ax_ratio.set_ylabel("Ratio")

  plotID = kwargs["plotID"]
  fig.savefig(saveFolder + f"/{plotID}.png")
  fig.savefig(saveFolder + f"/{plotID}.pdf")
  pkl.dump(fig, open(saveFolder + f"/{plotID}.pickle",  'wb')  ) 

  if ("show" in kwargs) and (kwargs["show"]): plt.show()


if __name__ == "__main__":

  def format_plot1(ax):
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"($\mathrm{p}_{\mathrm{T}}^{\mathrm{L1T}}$ $-$ $\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$)/$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$ ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([-0.9, 0.9])
    ax.set_ylim([0, 0.15])
    ax.axvline(x=0, linewidth=2, linestyle="--", c="tab:orange")
  
  # compareDatasets(
  #   binIdx = 5,
  #   binning_var = "genJetPt",
  #   binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "Calibrated_CaloJet_PU200_Puppi_PFCandidates_5x5", 
  #     "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
  #     "Calibrated_CaloJet_PU200_Puppi_PFCandidates_9x9",
  #     "Calibrated_AK4Jet_PU200_Puppi_PFCandidates",
  #   ],
  #   title = r"QCD+TTbar, $|\eta| < 5$",
  #   legend_labels = [
  #     r"Histogrammed jet $0.4\times0.4$",
  #     r"Histogrammed jet $0.6\times0.6$" ,
  #     r"Histogrammed jet $0.8\times0.8$",
  #     r"AK4"
  #   ],
  #   save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
  #   dataframe_paths = [
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #   ],
  #   # show = True,
  #   style = format_plot1,
  #   plotID = "jetSizeComparison",
  #   normalise = True
  # )
  
  def format_plot_of_cluster_size_comp(ax):
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"($\mathrm{p}_{\mathrm{T}}^{\mathrm{L1T}}$ $-$ $\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$)/$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$ ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([-0.9, 0.9])
    ax.set_ylim([0, 0.15])
    ax.axvline(x=0, linewidth=2, linestyle="--", c="tab:orange")
  
  # compareDatasets(
  #   binIdx = 5,
  #   binning_var = "genJetPt",
  #   binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "Calibrated_CaloJet_PU200_PfClusters_0.4x0.4",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8_PUS",
  #     "Calibrated_AK4Jet_PU200_PfClusters",
  #   ],
  #   title = r"QCD, $|\eta| < 5$",
  #   legend_labels = [
  #     r"Histogrammed jet $0.4\times0.4$" ,
  #     r"Histogrammed jet Phase-1",
  #     r"Histogrammed jet Phase-1 w/ PUS",
  #     r"AK4"
  #   ],
  #   save_folder = "JetFromPFClusters/",
  #   dataframe_paths = [
  #     "JetFromPFClusters/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #   ],
  #   # show = True,
  #   style = format_plot_of_cluster_size_comp,
  #   plotID = "jetSizeComparison",
  #   normalise = True
  # )
  # compareDatasets(
  #   binIdx = 5,
  #   binning_var = "genJetPt",
  #   binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "Calibrated_CaloJet_PU200_PfClusters_0.4x0.4",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8_PUS",
  #     "Calibrated_AK4Jet_PU200_PfClusters",
  #   ],
  #   title = r"QCD, $|\eta| < 1.4$",
  #   legend_labels = [
  #     r"Histogrammed jet $0.4\times0.4$" ,
  #     r"Histogrammed jet Phase-1",
  #     r"Histogrammed jet Phase-1 w/ PUS",
  #     r"AK4"
  #   ],
  #   save_folder = "JetFromPFClusters/",
  #   dataframe_paths = [
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
  #   ],
  #   # show = True,
  #   style = format_plot_of_cluster_size_comp,
  #   plotID = "jetSizeComparisonBarrel",
  #   normalise = True
  # )
  # compareDatasets(
  #   binIdx = 5,
  #   binning_var = "genJetPt",
  #   binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "Calibrated_CaloJet_PU200_PfClusters_0.4x0.4",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8_PUS",
  #     "Calibrated_AK4Jet_PU200_PfClusters",
  #   ],
  #   title = r"QCD, $1.4 < |\eta| < 2$",
  #   legend_labels = [
  #     r"Histogrammed jet $0.4\times0.4$" ,
  #     r"Histogrammed jet Phase-1",
  #     r"Histogrammed jet Phase-1 w/ PUS",
  #     r"AK4"
  #   ],
  #   save_folder = "JetFromPFClusters/",
  #   dataframe_paths = [
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
  #   ],
  #   # show = True,
  #   style = format_plot_of_cluster_size_comp,
  #   plotID = "jetSizeComparisonEndcap1",
  #   normalise = True
  # )
  # compareDatasets(
  #   binIdx = 5,
  #   binning_var = "genJetPt",
  #   binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "Calibrated_CaloJet_PU200_PfClusters_0.4x0.4",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8",
  #     "Calibrated_CaloJet_PU200_PfClusters_0.8x0.8_PUS",
  #     "Calibrated_AK4Jet_PU200_PfClusters",
  #   ],
  #   title = r"QCD, $2.4 < |\eta| < 3$",
  #   legend_labels = [
  #     r"Histogrammed jet $0.4\times0.4$" ,
  #     r"Histogrammed jet Phase-1",
  #     r"Histogrammed jet Phase-1 w/ PUS",
  #     r"AK4"
  #   ],
  #   save_folder = "JetFromPFClusters/",
  #   dataframe_paths = [
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
  #   ],
  #   # show = True,
  #   style = format_plot_of_cluster_size_comp,
  #   plotID = "jetSizeComparisonEndcap2",
  #   normalise = True
  # )

  def format_plot_of_cluster_size_comp_forward(ax):
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"($\mathrm{p}_{\mathrm{T}}^{\mathrm{L1T}}$ $-$ $\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$)/$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$ ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([-1, 1.9])
    ax.set_ylim([0, 0.3])
    ax.axvline(x=0, linewidth=2, linestyle="--", c="tab:orange")
  
  def format_plot_of_candidates_size_comp_forward(ax):
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"($\mathrm{p}_{\mathrm{T}}^{\mathrm{L1T}}$ $-$ $\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$)/$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$ ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([-1, 1.9])
    ax.set_ylim([0, 0.15])
    ax.axvline(x=0, linewidth=2, linestyle="--", c="tab:orange")

  # compareDatasets(
  #   binIdx = 5,
  #   binning_var = "genJetPt",
  #   binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "Uncalibrated_CaloJet_PU200_PfClusters_0.4x0.4",
  #     "Uncalibrated_CaloJet_PU200_PfClusters_0.8x0.8",
  #     "Uncalibrated_CaloJet_PU200_PfClusters_0.8x0.8_PUS",
  #     "Uncalibrated_AK4Jet_PU200_PfClusters",
  #   ],
  #   title = r"QCD, 3 < $|\eta| < 5$",
  #   legend_labels = [
  #     r"Histogrammed jet $0.4\times0.4$" ,
  #     r"Histogrammed jet Phase-1",
  #     r"Histogrammed jet Phase-1 w/ PUS",
  #     r"AK4"
  #   ],
  #   save_folder = "JetFromPFClusters/",
  #   dataframe_paths = [
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
  #     "JetFromPFClusters/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
  #   ],
  #   show = True,
  #   style = format_plot_of_cluster_size_comp_forward,
  #   plotID = "jetSizeComparisonForward",
  #   normalise = True
  # )

  compareDatasets(
    binIdx = 5,
    binning_var = "genJetPt",
    binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
    x_axis_var = "deltaPtOverPt",
    dataset_list = [
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_5x5", 
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_9x9",
      "Calibrated_AK4Jet_PU200_Puppi_PFCandidates",
    ],
    title = r"QCD, $|\eta| < 1.4$",
    legend_labels = [
      r"Histogrammed jet $0.4\times0.4$",
      r"Histogrammed jet $0.6\times0.6$" ,
      r"Histogrammed jet $0.8\times0.8$",
      r"AK4"
    ],
    save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
    dataframe_paths = [
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerBarrel.csv",
    ],
    # show = True,
    style = format_plot_of_cluster_size_comp,
    plotID = "jetSizeComparisonBarrelPfCandidates",
    normalise = True
  )
  compareDatasets(
    binIdx = 5,
    binning_var = "genJetPt",
    binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
    x_axis_var = "deltaPtOverPt",
    dataset_list = [
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_5x5", 
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_9x9",
      "Calibrated_AK4Jet_PU200_Puppi_PFCandidates",
    ],
    title = r"QCD, $1.4 < |\eta| < 2$",
    legend_labels = [
      r"Histogrammed jet $0.4\times0.4$",
      r"Histogrammed jet $0.6\times0.6$" ,
      r"Histogrammed jet $0.8\times0.8$",
      r"AK4"
    ],
    save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
    dataframe_paths = [
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap1.csv",
    ],
    # show = True,
    style = format_plot_of_cluster_size_comp,
    plotID = "jetSizeComparisonEndcap1PfCandidates",
    normalise = True
  )
  compareDatasets(
    binIdx = 5,
    binning_var = "genJetPt",
    binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
    x_axis_var = "deltaPtOverPt",
    dataset_list = [
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_5x5", 
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_9x9",
      "Calibrated_AK4Jet_PU200_Puppi_PFCandidates",
    ],
    title = r"QCD, $2.4 < |\eta| < 3$",
    legend_labels = [
      r"Histogrammed jet $0.4\times0.4$",
      r"Histogrammed jet $0.6\times0.6$" ,
      r"Histogrammed jet $0.8\times0.8$",
      r"AK4"
    ],
    save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
    dataframe_paths = [
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerEndcap2.csv",
    ],
    # show = True,
    style = format_plot_of_cluster_size_comp,
    plotID = "jetSizeComparisonEndcap2PfCandidates",
    normalise = True
  )

  compareDatasets(
    binIdx = 5,
    binning_var = "genJetPt",
    binning_var_label = r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}} $ [GeV]",
    x_axis_var = "deltaPtOverPt",
    dataset_list = [
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_5x5", 
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
      "Calibrated_CaloJet_PU200_Puppi_PFCandidates_9x9",
      "Calibrated_AK4Jet_PU200_Puppi_PFCandidates",
    ],
    title = r"QCD, 3 < $|\eta| < 5$",
    legend_labels = [
      r"Histogrammed jet $0.4\times0.4$",
      r"Histogrammed jet $0.6\times0.6$" ,
      r"Histogrammed jet $0.8\times0.8$",
      r"AK4"
    ],
    save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
    dataframe_paths = [
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
      "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.deltaPtOverPt--DeltaPtOverPtBinnerForward.csv",
    ],
    show = True,
    style = format_plot_of_candidates_size_comp_forward,
    plotID = "jetSizeComparisonForwardPfCandidates",
    normalise = True
  )

  # def format_plot2(ax):
  #   ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
  #   ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
  #   ax.xaxis.set_label_coords(0.98, -0.075)
  #   ax.set_xlabel(r"($\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ L1T}}$ $-$ $\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$)/$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$ ", horizontalalignment="right")
  #   ax.yaxis.set_label_coords(-0.1, 0.95)
  #   ax.set_ylabel(r"a.u.")
  #   ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
  #   ax.set_xlim([-1, 1.5])
  #   ax.set_ylim([0, 0.15])

  # compareDatasets(
  #   binIdx = 3,
  #   binning_var = "genMET",
  #   binning_var_label=r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$ (GeV)",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "MET_Puppi_PFCandidates_7x7_TTBar_PU200",
  #     "MET_Puppi_PFCandidates_7x7_TTBar_PU200_UnbinnedSinCos",
  #   ],
  #   title = "QCD+TTbar, $|\eta| < 5$",
  #   legend_labels = [
  #     r"MET binned sin-cos",
  #     r"MET unbinned sin-cos",
  #   ],
  #   save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
  #   dataframe_paths = [
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genMET.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genMET.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #   ],
  #   show = False,
  #   style = format_plot2,
  #   plotID = "binnedVSUnbinnedComparison"
  # )

  def format_plot3(ax):
    ax.annotate(r"14 TeV, 3000 fb$^{-1}$", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.075)
    ax.set_xlabel(r"($\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ L1T}}$ $-$ $\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$)/$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$ ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([0, 200])
    ax.set_ylim([0, 100])

  # compareDatasets(
  #   binning_var = "genMET",
  #   binning_var_label=r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss\ gen}}$ (GeV)",
  #   x_axis_var = "deltaPtOverPt",
  #   dataset_list = [
  #     "MET_Puppi_PFCandidates_7x7_TTBar_PU200",
  #     "MET_PF_PFCandidates_7x7_TTBar_PU200",
  #   ],
  #   title = "TTbar, 200 PU\n$|\eta| < 5$",
  #   legend_labels = [
  #     r"Puppi MET",
  #     r"PF MET",
  #   ],
  #   save_folder = "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
  #   dataframe_paths = [
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genMET.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #     "PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genMET.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  #   ],
  #   style = format_plot3,
  #   plotID = "MET_PFCandidates_7x7_TTbar_PU200_PF_VS_PUPPI"
  # )

  # compareDatasets(
  #   x_axis_var = "l1tMHT",
  #   dataset_list = [
  #     "MHT_CMSSW",
  #     "MHT_HLS",
  #   ],
  #   title = "TTbar, 200 PU\n$|\eta| < 5$",
  #   legend_labels = [
  #     r"CMSSW emu",
  #     r"HLS emu",
  #   ],
  #   save_folder = "MHT_analysis",
  #   dataframe_paths = [
  #     "MHT_analysis/tbl_dataset.l1tMHT--PtDistributionBinner.csv",
  #     "MHT_analysis/tbl_dataset.l1tMHT--PtDistributionBinner.csv",
  #   ],
  #   style=format_plot3,
  #   plotID = "MHT_Distribution_Comparison"
  # )

  def format_plot_ht_comparison(ax):
    ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.055)
    ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}$ (GeV) ", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([25, 250])
    ax.set_ylim([1, 1000])
    ax.set_yscale("log")

  # compareDatasets(
  #   x_axis_var = "ht",
  #   dataset_list = [
  #     "Sums",
  #     "Sums",
  #   ],
  #   title = "TTbar\n50000 events",
  #   legend_labels = [
  #     r"Emu",
  #     r"HW",
  #   ],
  #   save_folder = "HW_EMU_Test",
  #   dataframe_paths = [
  #     "HW_EMU_Test/tbl_dataset.ht--emuHTDistributionBinner.csv",
  #     "HW_EMU_Test/tbl_dataset.ht--hwHTDistributionBinner.csv",
  #   ],
  #   style=format_plot_ht_comparison,
  #   plotID = "HT_Distribution_Comparison",
  #   include_ratio = True
  # )

  def format_plot_met_comparison(ax):
    ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.055)
    ax.set_xlabel(r"$\mathrm{E}_{\mathrm{T}}^{\mathrm{miss}}$ (GeV)", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([4, 150])
    ax.set_ylim([10, 10000])
    ax.set_yscale("log")

  # compareDatasets(
  #   x_axis_var = "met",
  #   dataset_list = [
  #     "Sums",
  #     "Sums",
  #   ],
  #   title = "TTBar\n50000 events",
  #   legend_labels = [
  #     r"Emu",
  #     r"HW",
  #   ],
  #   save_folder = "HW_EMU_Test",
  #   dataframe_paths = [
  #     "HW_EMU_Test/tbl_dataset.met--emuMETDistributionBinner.csv",
  #     "HW_EMU_Test/tbl_dataset.met--hwMETDistributionBinner.csv",
  #   ],
  #   style=format_plot_met_comparison,
  #   plotID = "MET_Distribution_Comparison",
  #   include_ratio = True
  # )

  def format_plot_mht_comparison(ax):
    ax.annotate(r"14 TeV, PU 200", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
    ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
    ax.xaxis.set_label_coords(0.98, -0.055)
    ax.set_xlabel(r"$\mathrm{H}_{\mathrm{T}}^{\mathrm{miss}}$ (GeV)", horizontalalignment="right")
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.set_ylabel(r"a.u.")
    # ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.set_xlim([4, 150])
    ax.set_ylim([1, 3500])
    ax.set_yscale("log")

  # compareDatasets(
  #   x_axis_var = "mht",
  #   dataset_list = [
  #     "Sums",
  #     "Sums",
  #   ],
  #   title = "TTBar\n50000 events",
  #   legend_labels = [
  #     r"Emu",
  #     r"HW",
  #   ],
  #   save_folder = "HW_EMU_Test",
  #   dataframe_paths = [
  #     "HW_EMU_Test/tbl_dataset.mht--emuMHTDistributionBinner.csv",
  #     "HW_EMU_Test/tbl_dataset.mht--hwMHTDistributionBinner.csv",
  #   ],
  #   style=format_plot_mht_comparison,
  #   plotID = "MHT_Distribution_Comparison",
  #   include_ratio = True
  # )
