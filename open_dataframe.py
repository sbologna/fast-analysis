#! /usr/bin/env python

from fast_plotter.utils import *
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

dataframe = read_binned_df(sys.argv[1])
