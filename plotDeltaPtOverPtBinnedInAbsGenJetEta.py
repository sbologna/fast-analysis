''' Plots the pt resolution binned in the gen jet pt

  Requires a dataframe genJetPt and deltaPtOverPt binning variables and n

'''
from fast_plotter.utils import read_binned_df
from fast_plotter.interval_from_str import *
from fast_plotter.plotting import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cycler import cycler
import pickle as pkl
from math import sqrt
from math import isnan

def plotDeltaPtOverPtBinnedInAbsGenJetEta(*args, **kwargs):

  # plt.rc('text', usetex=True)
  plt.rc('font', family='serif', size=12)
  plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))
  plt.rcParams['font.family'] = 'sans-serif'
  plt.rcParams['font.sans-serif'] = 'Helvetica'
  datasetsAndTitle = {}
  for x in range(0, len(kwargs["datasets"])):
    datasetsAndTitle[kwargs["datasets"][x]] = kwargs["titles"][x]  

  binIdxs = kwargs["eta_index"] if "eta_index" in kwargs else None
  saveFolder = kwargs["save_folder"]


  dataframe = read_binned_df(kwargs["dataframe_path"])

  #select eta range, if any, otherwise go full detector
  if "pt_index" in kwargs:
    ptInterval = dataframe.index.levels[3][kwargs["pt_index"]]
    dataframe = dataframe.xs(etaInterval, level=3)

  #grouping by dataset and genjet eta
  if binIdxs is not None:
    grouped_dataframe = dataframe.groupby(["dataset","absGenJetEta", "deltaPtOverPt"]).sum()
    grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[2].mid, level="deltaPtOverPt", inplace=True)
  else:
    grouped_dataframe = dataframe.groupby(["dataset", "deltaPtOverPt"]).sum()
    grouped_dataframe.index.set_levels(grouped_dataframe.index.levels[1].mid, level="deltaPtOverPt", inplace=True)
  # replacing interval in genjet eta with central value

  # creating window and sub plot
  #ax.set_prop_cycle(cycler('color', ['r', 'g', 'b', 'y']))

  figs = []
  axs = []

  for dataset, description in datasetsAndTitle.items():
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    figs.append(fig)
    axs.append(ax)
    # preparing dataframe with columns to plot
    # selecting indexes
    if binIdxs is not None:
      idxs = [grouped_dataframe.index.levels[1][binIdx] for binIdx in binIdxs]
    else:
      idxs = [None]
    
    legendList = []

    for absGenJetEtaInterval in idxs:
      #trimming the dataframe
      dataframe_plot = grouped_dataframe.xs(dataset)
      if absGenJetEtaInterval is not None:
        dataframe_plot = dataframe_plot.xs(absGenJetEtaInterval)
      col_total = dataframe_plot["n"].sum()
      dataframe_plot["n"] = dataframe_plot["n"]/col_total
      dataframe_plot = dataframe_plot.fillna(0)
    # dataframe_plot.plot(ax=ax, linestyle="None", marker=".", y="n", yerr="n_err", legend=False)
      # dataframe_plot.plot(ax=ax, y="n", width=1, drawstyle="step", linestyle="-")
      # import pdb ; pdb.set_trace()
      ax.step(dataframe_plot.index.values, dataframe_plot["n"], where="pre") 
      ax.annotate(r"14 TeV, 3000 fb$^{-1}$, 200 PU", xycoords='axes fraction', xy=(1, 1.01), horizontalalignment="right")
      ax.annotate(description, xycoords='axes fraction', xy=(0.99, 0.53), horizontalalignment="right")
      ax.annotate(r"$\bf{CMS}$ Phase-2 simulation", xycoords='axes fraction', xy=(0, 1.01))
      ax.xaxis.set_label_coords(0.98, -0.075)
      ax.set_xlabel(r"($\mathrm{p}_{\mathrm{T}}^{\mathrm{L1T}}$ $-$ $\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$)/$\mathrm{p}_{\mathrm{T}}^{\mathrm{gen}}$ ", horizontalalignment="right")
      ax.yaxis.set_label_coords(-0.1, 0.95)
      ax.set_ylabel(r"a.u.")
    
      #replacing NaNs (empty lines) with 0
      #removing under- and overflow, if existing, to have finite average
      # import pdb ; pdb.set_trace()
      if float("-inf") in dataframe_plot.index: dataframe_plot.loc[float("-inf")] = 0
      if float("+inf") in dataframe_plot.index: dataframe_plot.loc[float("+inf")] = 0
      delta_pt_avg = (dataframe_plot.index * dataframe_plot["n"]).sum()
      delta_pt_squared_avg = (dataframe_plot.index**2 * dataframe_plot["n"]).sum()
      #import pdb; pdb.set_trace()
      rms = round(sqrt(delta_pt_squared_avg - delta_pt_avg**2), 2)
      avg = round(delta_pt_avg, 2)
      if absGenJetEtaInterval is not None:
        legendEntry = (str(absGenJetEtaInterval.left) if str(absGenJetEtaInterval.left) != "-inf" else "$-\\infty$") + \
                      " $ < |\eta| $ $ < $ " + \
                      (str(absGenJetEtaInterval.right) if str(absGenJetEtaInterval.right) != "inf" else "$\\infty$")
                      # ", n: " + str(col_total) + \
                      # (", $\mu$: " + str(avg) if not isnan(avg) else "") + \
                      # (", $\sigma$: " + str(rms))
        legendList.append(legendEntry)

    ax.grid(True, linestyle="dashed", linewidth=0.2, color="black")
    ax.legend(legendList, labelspacing=0.3)
    ax.set_xlim([-1,1])
    ax.set_ylim([0, 0.15])

    fig.savefig(saveFolder + "/deltaPtOverPtBinnedInabsGenJetEta_" + dataset + ".png")
    fig.savefig(saveFolder + "/deltaPtOverPtBinnedInabsGenJetEta_" + dataset + ".pdf")
    pkl.dump(fig, open(saveFolder + "/deltaPtOverPtBinnedInabsGenJetEta_" + dataset + ".pickle",  'wb')  )  

  if ("show" in kwargs) and (kwargs["show"]): plt.show()

if __name__ == "__main__":

  # plotDeltaPtOverPtBinnedInAbsGenJetEta(
  # datasets=[
  #   "Calibrated_CaloJet_PU200_Puppi_PFCandidates_3x3",
  #   "Calibrated_CaloJet_PU200_Puppi_PFCandidates_5x5",
  #   "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
  #   "Calibrated_CaloJet_PU200_Puppi_PFCandidates_9x9",
  #   "Calibrated_AK4Jet_PU200_Puppi_PFCandidates",
  # ],
  # titles=[
  #   "Histogrammed jet 3x3, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV",
  #   "Histogrammed jet 5x5, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV",
  #   "Histogrammed jet 7x7, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV",
  #   "Histogrammed jet 9x9, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV",
  #   "AK4, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV"
  # ],
  # eta_index=[0, 1, 2, 3],
  # save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
  # dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  # show=True,
  # )
  
  plotDeltaPtOverPtBinnedInAbsGenJetEta(
  datasets=[
    "Calibrated_CaloJet_PU200_Puppi_PFCandidates_7x7",
  ],
  titles=[
    "Histogrammed jet, QCD+TTbar\n" + r"$\mathrm{p}_{\mathrm{T}}^{\mathrm{L1T}}$ $ > 40$ GeV",
  ],
  eta_index=[0, 1, 2, 3],
  save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/",
  dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  show=True,
  )

  # plotDeltaPtOverPtBinnedInAbsGenJetEta(
  # datasets=["Calibrated_CaloJet_PU200_Puppi_PFCandidates", "Calibrated_AK4Jet_PU200_Puppi_PFCandidates"],
  # titles=["Histogrammed jet 9x9, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV", "AK4, QCD+TTbar, L1T $\mathrm{p}_{\mathrm{T}}$ $ > 40$ GeV"],
  # eta_index=[0, 1, 2, 3],
  # save_folder="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_9x9Jets_full_detector/",
  # dataframe_path="PU200_CalibratedPhase1AndAK4L1TJetsFromPfCandidates_10_0_4_MTD_9x9Jets_full_detector/tbl_dataset.genJetPt.absGenJetEta.deltaPtOverPt--DeltaPtOverPtBinner.csv",
  # show=True,
  # )

  pass